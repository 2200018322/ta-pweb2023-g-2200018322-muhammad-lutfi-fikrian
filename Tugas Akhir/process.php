<!-- process.php -->
<?php
// Fungsi untuk membersihkan dan memvalidasi data input
function cleanInput($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

// Fungsi untuk menyimpan data user ke dalam file
function saveUser($username, $email, $password) {
  $filename = 'users.txt';
  $data = "$username,$email,$password\n";
  file_put_contents($filename, $data, FILE_APPEND | LOCK_EX);
}

// Fungsi untuk memeriksa apakah email sudah terdaftar
function isEmailRegistered($email) {
  $filename = 'users.txt';
  $users = file($filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
  foreach ($users as $user) {
    $user_data = explode(',', $user);
    if ($user_data[1] === $email) {
      return true;
    }
  }
  return false;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (isset($_POST['login'])) {
    // Proses login
    $email = cleanInput($_POST['email']);
    $password = cleanInput($_POST['password']);

    $filename = 'users.txt';
    $users = file($filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

    foreach ($users as $user) {
      $user_data = explode(',', $user);
      if ($user_data[1] === $email && $user_data[2] === $password) {
        // Login berhasil
        header('Location: welcome.php'); // Ganti welcome.php dengan halaman selamat datang setelah login berhasil
        exit;
      }
    }

    // Jika login gagal, bisa tambahkan pesan error atau redirect kembali ke halaman login
    header('Location: index.html');
    exit;
  } elseif (isset($_POST['register'])) {
    // Proses registrasi
    $username = cleanInput($_POST['username']);
    $email = cleanInput($_POST['email']);
    $password = cleanInput($_POST['password']);

    // Periksa apakah email sudah terdaftar
    if (isEmailRegistered($email)) {
      // Jika email sudah terdaftar, bisa tambahkan pesan error atau redirect kembali ke halaman registrasi
      header('Location: index.html');
      exit;
    }

    // Simpan data user ke dalam file
    saveUser($username, $email, $password);

    // Setelah berhasil registrasi, bisa tambahkan pesan sukses atau redirect ke halaman selamat datang setelah registrasi
    header('Location: welcome.php'); // Ganti welcome.php dengan halaman selamat datang setelah registrasi berhasil
    exit;
  }
}
?>
