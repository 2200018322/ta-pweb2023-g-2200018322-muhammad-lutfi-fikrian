<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Welcome to Forest</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

  <header>
    <h2 class="logo"><img src="logo.png"></h2>
    <nav class="navigation">
      <a href="#">Home</a>
      <a href="#">About</a>
      <a href="#">Services</a>
      <a href="#">Contact</a>
      <button class="btnLogout">Logout</button>
    </nav>
  </header>

  <div class="welcome-message">
    <h1>Welcome to Forest!</h1>
    <p>Thank you for joining us. You are now logged in.</p>
  </div>

  <script src="script.js"></script>
  <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
  <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
</body>
</html>
