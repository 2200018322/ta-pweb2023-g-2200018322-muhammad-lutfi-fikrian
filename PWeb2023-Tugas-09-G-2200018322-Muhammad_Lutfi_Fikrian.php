<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP</title>
</head>
<body>
	<h2>Contoh PHP</h2>

	<?php
		$gaji = 1000000;
		$pajak = 0.1;
		$thp = $gaji - ($gaji * $pajak);

		echo "Gaji sebelum pajak = Rp. $gaji <br>";
		echo "Gaji yang dibawa pulang = Rp. $thp";
	?>

	<hr>

	<h2>Operator Perbandingan</h2>
	<?php
		$a = 5;
		$b = 4;

		echo "$a == $b: " . ($a == $b);
		echo "<br>$a != $b: " . ($a != $b);
		echo "<br>$a > $b: " . ($a > $b);
		echo "<br>$a < $b: " . ($a < $b);
		echo "<br>($a == $b) && ($a > $b): " . (($a == $b) && ($a > $b));
		echo "<br>($a == $b) || ($a > $b): " . (($a == $b) || ($a > $b));
	?>

	<hr>

	<h2>Operasi Aritmatika Dasar</h2>
	<?php
		$num1 = 10;
		 $num2 = 5;

		$penjumlahan = $num1 + $num2;
		$pengurangan = $num1 - $num2;
		$perkalian = $num1 * $num2;
		$pembagian = $num1 / $num2;

		echo "Penjumlahan: $num1 + $num2 = $penjumlahan";
		echo "<br>Pengurangan: $num1 - $num2 = $pengurangan";
		echo "<br>Perkalian: $num1 * $num2 = $perkalian";
		echo "<br>Pembagian: $num1 / $num2 = $pembagian";
	?>

	<hr>

	<h2>Perulangan</h2>
	<?php
		echo "Menggunakan perulangan for untuk mencetak angka dari 1 hingga 5:<br>";
		for ($i = 1; $i <= 5; $i++) {
			echo "$i ";
		}

		echo "<br><br>Menggunakan perulangan while untuk mencetak angka dari 1 hingga 5:<br>";
		$j = 1;
		while ($j <= 5) {
			echo "$j ";
			$j++;
		}
	?>

	<hr>

	<h2>Array</h2>
	<?php
		$mobil = array("Volvo", "BMW", "Toyota");

		echo "Mencetak elemen array menggunakan perulangan for:<br>";
		for ($i = 0; $i < count($mobil); $i++) {
			echo "$mobil[$i] ";
		}

		echo "<br><br>Mencetak elemen array menggunakan perulangan foreach:<br>";
		foreach ($mobil as $mobil) {
			echo "$mobil ";
		}
	?>

	<hr>

	<h2>Perhitungan Kuadrat dan Akar Kuadrat</h2>
	<?php
		$angka = 9;

		$kuadrat = $angka * $angka;
		$akar_kuadrat = sqrt($angka);

		echo "Angka: $angka";
		echo "<br>Kuadrat: $kuadrat";
		echo "<br>Akar Kuadrat: $akar_kuadrat";
	?>
</body>
</html>
